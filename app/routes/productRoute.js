//khai bao thu vien express
const express = require("express")

// Import product controller
const productController = require("../controllers/productController");

//Tạo ra router
const productRouter = express.Router();

productRouter.post('/products', productController.createProduct);
productRouter.get('/products', productController.getAllProduct);
productRouter.get('/products/:id', productController.getProductById);
productRouter.put('/products/:id', productController.updateProductById);
productRouter.delete('/products/:id', productController.deleteProduct);
   
    module.exports = { productRouter }