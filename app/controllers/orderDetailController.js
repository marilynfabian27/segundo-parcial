//import model
const orderDetailModel = require("../models/orderDetailModel");



const createOrderDetail= async (req, res)=> {
    try {
      const { order, product, quantity, price } = req.body;
  
      // Tạo một instance user mới từ model User
      const orderDetail = new orderDetailModel({
        order,
        product,
        quantity,
        price
        
      });
  
      // Lưu order detail vào database
      const savedOrderDetail = await orderDetail.save();
  
      res.status(201).json(savedOrderDetail);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  }


const getAllOrderDetail = async () => {
    try {
        const orderDetails = await orderDetailModel.find();
        return orderDetails;
    } catch (error) {
        throw new Error(error);
    }
}

const getOrderDetailById = async (req, res) => {
    try {
        const orderDetail = await orderDetailModel.findById(req.params.id)
            .populate("order")
            .populate("product");

        if (!orderDetail) {
            return res.status(404).json({ message: "Order detail not found" });
        }

        res.status(200).json(orderDetail);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Server error" });
    }
};

const updateOrderDetailById = async (id, data) => {
    try {
        const updatedOrderDetail = await orderDetailModel.findByIdAndUpdate(id, data, {
            new: true
        });
        return updatedOrderDetail;
    } catch (error) {
        console.log(error);
        throw new Error("Error while updating order detail.");
    }
};

const deleteOrderDetailById = async (id) => {
    try {
        const orderDetail = await orderDetailModel.findByIdAndDelete(id);
        if (!orderDetail) {
            throw new Error(`Order detail with ID ${id} not found`);
        }
        return orderDetail;
    } catch (error) {
        throw new Error(`Error deleting order detail with ID ${id}: ${error.message}`);
    }
};

module.exports = {
    createOrderDetail,
    getAllOrderDetail,
    getOrderDetailById,
    updateOrderDetailById,
    deleteOrderDetailById,
}