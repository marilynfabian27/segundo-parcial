//import model
const orderModel = require("../models/orderModel");

// Create a new order - Tạo đơn hàng mới
const createOrder = async (req, res) => {
    try {
        const { customer, products, total } = req.body;

        // Tạo một instance đơn hàng mới từ model Order
        const order = new orderModel({
            customer,
            products,
            total
        });

        // Lưu đơn hàng vào database
        const savedOrder = await order.save();

        res.status(201).json(savedOrder);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
}

const getAllOrder = async (req, res) => {
    try {
        const orders = await orderModel.find().populate("customer products.product payment");
        res.json(orders);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}


const getOrderById = async (req, res) => {
    try {
        const order = await orderModel.findById(req.params.id)
            .populate("customer")
            .populate({
                path: "products.product",
                model: "Product"
            })
            .populate("payment");

        if (!order) {
            return res.status(404).json({ message: "Order not found" });
        }

        res.status(200).json(order);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

const updateOrderById = async (req, res) => {
    try {
        const updatedOrder = await orderModel.findByIdAndUpdate(
            req.params.id,
            req.body,
            { new: true }
        );

        if (!updatedOrder) {
            return res
                .status(404)
                .json({ message: "Cannot find order with given ID" });
        }

        res.status(200).json(updatedOrder);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
}

const deleteOrderById = async (req, res) => {
    const orderId = req.params.orderId;

    try {
        // Check if the order exists
        const order = await orderModel.findById(orderId);
        if (!order) {
            return res.status(404).json({ message: "Order not found" });
        }

        // Delete the order
        await Order.findByIdAndDelete(orderId);

        res.json({ message: "Order deleted successfully" });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};
module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}