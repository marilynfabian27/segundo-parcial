//khai báo mongoose
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance promotionSchema từ Class Schema
const promotionSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true
    },
    discount: {
        type: Number,
        required: true
    },
    expiredAt: {
        type: Date,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Promotion", promotionSchema);