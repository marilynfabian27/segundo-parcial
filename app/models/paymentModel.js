//khai báo mongoose
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance paymentSchema từ Class Schema
const paymentSchema = new Schema({
    customer: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    method: {
        type: String,
        enum: ["Cash", "Credit Card"],
        required: true
    },
    status: {
        type: String,
        enum: ["Pending", "Paid"],
        default: "Pending"
    },
    amount: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Payment", paymentSchema);