//Khai báo thư viện
const express = require("express");

//Khai báo thư viện path
const path = require("path");

//khai báo mongoose
const mongoose = require("mongoose");

//Khai báo app
const app = express();

//Khai báo cổng
const port = 8000;

//Khai báo router app
const { userRouter } = require("./app/routes/userRoute");
const { orderRouter } = require("./app/routes/orderRoute");
const { productRouter } = require("./app/routes/productRoute");
const { promotionRouter } = require("./app/routes/promotionRoute");
const { paymentRouter } = require("./app/routes/paymentRoute");
const { orderDetailRouter } = require("./app/routes/orderDetailRoute");


// Khai báo để sử dụng body json
app.use(express.json());

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended:true
}))

const cors=require("cors");
const corsOptions ={
   origin:'*', 
   credentials:true,            //access-control-allow-credentials:true
   optionSuccessStatus:200,
}

app.use(cors(corsOptions))
app.use("/", userRouter);
app.use("/", orderRouter);
app.use("/", productRouter);
app.use("/", promotionRouter);
app.use("/", paymentRouter);
app.use("/", orderDetailRouter);


//call api chạy project 
app.get("/",(request, response) =>{
  response.sendFile(path.join(__dirname + "/views/home.html"))
})

app.get("/admin",(request, response) =>{
  response.sendFile(path.join(__dirname + "/views/admin.html"))
})

//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"));


// Kết nối với MongoDB:
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Project11")
 .then(() => {
    console.log('Successfully connected');
  })
  .catch((error) => {
    console.error(error);
  });


//Khởi động app
app.listen(port, () => {
    console.log(`App đang chạy trên cổng ${port}`);
})
